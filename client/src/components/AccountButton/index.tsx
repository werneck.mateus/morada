import { Button } from './styles';

export function MyAccountButton() {
    function handleMyAccountOpen() {
        return;
    }
    return (
        <Button size={50} cursor={'pointer'} id="customButton" onClick={handleMyAccountOpen}/>
    );

}