import styled from "styled-components";
import {MdOutlineAccountCircle} from 'react-icons/md';

export const Button = styled(MdOutlineAccountCircle)`
    color: var(--orange-100);
    transition: filter 0.2s;

    &:hover {
        filter: brightness(0.7);
    }

    @media (max-width: 720px) {
        height: 2.5rem;
    }


`