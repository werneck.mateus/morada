import { useRouter } from "next/dist/client/router";
import Link, { LinkProps } from "next/link";
import { ReactElement, cloneElement } from "react";

interface ActiveLinkProps extends LinkProps {
    children: ReactElement,
    activeClassName: string
}

export function ActiveLink({children, activeClassName, ...rest}: ActiveLinkProps) {
    const {asPath} = useRouter();
    let className = ""
    
    if(asPath === "/") {
        if(asPath === rest.href) {
            className = activeClassName
        }
    } else {
       const path = asPath.split("/")[1]
       if(String(rest.href).match(path)){
            className = activeClassName
       }
    }

    return (
        <Link {...rest}>
        {cloneElement(children, {
            className:className
        })}
        </Link>
    );
}
