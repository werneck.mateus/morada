import { ActiveLink } from "../ActiveLink";
import { Container, Instagram, WhatsApp, Buttons, Menu } from "./styles";

export function Footer() {

    async function handleInstagram() {




    }

    return (
        <>
            <Container>
                <Buttons>
                    <Instagram size={30} onClick={() => window.open("https://instagram.com/")} />
                    <WhatsApp size={30} onClick={() => window.open("https://api.whatsapp.com/send?phone=5531998040120")} />
                </Buttons>
                <Menu>
                    <ActiveLink activeClassName="active" href='/'>
                        <a>Início</a>
                    </ActiveLink>
                    <ActiveLink activeClassName="active" href='/imoveis'>
                        <a>Imoveis</a>
                    </ActiveLink>
                    <ActiveLink activeClassName="active" href='/sobre'>
                        <a>Sobre</a>
                    </ActiveLink>
                    <ActiveLink activeClassName="active" href='/contato'>
                        <a>Contato</a>
                    </ActiveLink>
                </Menu>

            </Container>
        </>
    )
}