import styled from 'styled-components';
import {BsInstagram, BsWhatsapp} from 'react-icons/bs';


export const Container = styled.div`
    display: flex;
    width: 100%;
    border: 1px solid var(--pink-80);
    margin-top: 40vh;
    background-color: var(--pink-80);
    align-items:center;


`

export const Buttons = styled.div ` 
    margin: 2.5rem;

    @media(max-width: 720px) {
        margin: 4.5rem;
    }


`

export const Menu = styled.div `
    display: grid;
    margin: 1rem;
    align-items: center;

    a {
        color: var(--orange-100);
        text-decoration:none;
        font-size: 0.8rem;
        transition: color 0.2s;

        & + a {
                margin-top: 1rem;

            }


        &:hover {
            color: var(--orange-200)
        }

        &.active {
                color:var(--pink-100);
                font-weight: bold;
            }
    }



`

export const Instagram = styled(BsInstagram) `
    margin: 0.5rem;
    color: #cd486b;
    cursor:pointer;

    @media(max-width: 720px){
        margin: 0 1rem;
        width: 50%;
    }

    &:hover {
        filter: brightness(0.7);
    }

`

export const WhatsApp = styled(BsWhatsapp) `
    margin: 0.5rem 1.5rem;
    color: #25D366;
    cursor: pointer;

    @media(max-width: 720px){
        margin: 2rem 1rem;
        width: 50%;
    }

    &:hover {
        filter: brightness(0.7);
    }

`