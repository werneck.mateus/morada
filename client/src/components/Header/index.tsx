import Link from "next/link";
import { MyAccountButton } from "../AccountButton";
import { ActiveLink } from "../ActiveLink";
import { SearchBar } from "../SearchBar";
import { Container, ButtonContainer, NavContainer, Morada, LogoContainer } from "./styles";



export function Header() {
    return (
        <>
            <Container>
                <LogoContainer>
                    <Link href='/'>
                        <Morada
                            src="/Morada.png"
                            width={100}
                            height={100}
                        />
                    </Link>
                </LogoContainer>
                <SearchBar />

            </Container>
            <NavContainer>
                <nav>
                    <ActiveLink activeClassName="active" href='/'>
                        <a style={{fontSize: '0.8rem'}}>Início</a>
                    </ActiveLink>
                    <ActiveLink activeClassName="active" href='/imoveis'>
                        <a style={{fontSize: '0.8rem'}}>Imoveis</a>
                    </ActiveLink>
                    <ActiveLink activeClassName="active" href='/sobre'>
                        <a style={{fontSize: '0.8rem'}}>Sobre</a>
                    </ActiveLink>
                </nav>
            </NavContainer>
        </>
    );
}