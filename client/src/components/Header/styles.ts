import Image from 'next/image';
import styled from 'styled-components';

export const Morada = styled(Image)`
  border-radius: 10px;
  

  &:hover {
      filter: brightness(0.8);
      cursor: pointer;
  }

`;

export const LogoContainer = styled.div`
    position:absolute;
    left: 20px;
    top:20px;

    @media(max-width: 1280px) {
        width: 10%;
        position: absolute;
        top: 10px;
        
    }

    @media(max-width: 720px) {
        width: 15%;
        position: absolute;
        top: 25px;
        
    }



`

export const Container = styled.header`
   display: flex;
   margin: auto;
   width: 100%;
   height: 7.5rem;
   background: white;
   justify-content: center;
   align-items: center;

   @media(max-width: 720px) {
        top:10px;
        margin-bottom: 2rem;
    }


`
export const ButtonContainer = styled.div`
    display: inline-flex;
    float: right;
    position: absolute;
    top: 35px;
    right: -30px;
    align-items: center;
    justify-content: right; 
    margin: auto 3.5rem;
    
    @media(max-width: 720px) {
        width: 25%;
        position: absolute;
        top: 25px;
        right: -40px;

    }

    @media(max-width: 1280px) {
        position: absolute;
        top: 30px;

    }

    #customButton {
        margin-left: 1.5rem;

        @media(max-width: 720px) {
            margin-left: 0.5rem;
        }
    }

`

export const NavContainer = styled.div`
    display: flex;
    border: 1px solid var(--pink-150);
    height: 4rem;
    margin-bottom: 3.5rem;
    align-items: center;
    justify-content: center;

    @media (max-width: 720px) {
        height: 2rem;
    }
    @media (max-width: 1280px) {
        position: relative;
        top: 30px;
    }
    

    nav {
        justify-content: center;
        align-items: center;
        text-align: center;

        a {
            display: inline-block;
            position: relative; //Importante para alinhar o &.active::after
            color:var(--orange-100);
            text-decoration: none;
            line-height: 5rem;
            font-size: 1.2rem;
            transition: color 0.2s;

            @media(max-width: 1280px) {
                    font-size: 1rem;
            }
            

            @media(max-width: 720px) {
                    font-size: 0.4rem;
            }

            

            &:hover {
                color: var(--orange-200)
            }

            & + a {
                margin-left: 2rem;

                @media(max-width: 720px) {
                    margin-left: 1rem;
                }
            }

            &.active {
                color:var(--pink-100);
                font-weight: bold;
            }
            &.active::after {
                    content: '';
                    height:3px;
                    border-radius:3px 3px 0 0;
                    width: 100%; //Muito importante
                    position: absolute; //Muito importante
                    bottom: 8px;
                    left: 1px;
                    background: var(--pink-100);
                
                    @media(max-width: 720px) {
                        bottom: 26px;
                        left: 0;
                    }

                }
        }
    }


`