import { useForm } from "react-hook-form"

interface InputProps {
    defaultValue: string,
    label: string,
    register: any,
}

export function Input({ defaultValue, label, register }: InputProps) {
    return (
        <label>
            <span style={{color: 'var(--gray-300)', fontSize: '0.75rem'}}>{label}</span>
            <br />
            <input style={{ marginRight: '1.5rem' }} value={defaultValue} {...register(label.toLowerCase())}/>
        </label>

    )
}