import { useRouter } from "next/router";
import { FormEvent, useState } from "react";
import { Input, FormContainer, Icon } from "./styles";
import $ from 'jquery';


export function SearchBar() {
    const router = useRouter();
    const [searchValue, setSearchValue] = useState<string>("")



    async function handleSearch(e: FormEvent) {
        e.preventDefault();
        if (searchValue === "" || searchValue === undefined) {
            return;
        }
        await router.push(`/resultado-de-busca/${searchValue}`)
        $('#busca').val('')
    }

    async function handleChange() {
        $('#busca').css('color', 'black')
    }


    return (
        <FormContainer onSubmit={(e: FormEvent) => handleSearch(e)}>
            <Input type='search' placeholder="Buscar" id="busca" onChange={(e) => { setSearchValue(e.target.value), handleChange()}} />
            <Icon size={20} onClick={(e) => handleSearch(e)}/>
        </FormContainer>
    );

}