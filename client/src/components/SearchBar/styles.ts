import styled from "styled-components";
import { Form } from "react-bootstrap";
import { AiOutlineSearch } from "react-icons/ai";

export const FormContainer = styled(Form)`
    display:grid;
    justify-content: center;
    align-items: center;
    
    @media(max-width: 1280px) {
        width: 100%;
        position: relative;
        top: 60px;
        
    }

    @media(max-width: 720px) {
        width: 100%;
        position: absolute;
        top: 120px;
        
    }
`

export const Input = styled.input`
    display: inline-block;
    height: 40px;
    max-width: 1920px;
    position: relative;
    top:10px;
   
    border: 1px solid var(--gray-50);
    border-radius: 10px;
    text-indent: 2rem;
    

    @media(max-width: 3840px) {
        width: 1920px;
    }

    @media(max-width: 1920px) {
        width: 800px;
    }

    @media(max-width: 1280px) {
        width: 600px;
        position: relative;
        left: -5px;
    }
    @media(max-width: 720px) {
        width: 320px;
        position: relative;
        left: 1px;
    }

    &:focus {
        border: 1px solid var(--purple-150);
        box-shadow: 1px 1px 5px var(--purple-150);
        
    }

    &::placeholder {
        color: var(--orange-100)
    }



`

export const Icon = styled(AiOutlineSearch)`
        position:relative;
        bottom:1.2rem;
        margin-left: 0.6rem;
        color: var(--gray-50);
        cursor: pointer;
        &:hover {
            filter: brightness(0.2)
        }

        @media (max-width: 1280px) {
            margin-left: 0.4rem;
        }

        @media (max-width: 720px) {
            margin-left: 0.5rem;
        }


`

