import React, { ReactElement } from "react";
import { Container } from "react-bootstrap";
import { useTable } from 'react-table';

interface TableDataProps {
  data: Array<any>
  columns: Array<{ Header: string, accessor: string, Cell?: (props: any) => ReactElement }>
}

export function TableData({ data, columns }: TableDataProps) {
  const tableInstance = useTable({ columns, data })

  const {

    getTableProps,

    getTableBodyProps,

    headerGroups,

    rows,

    prepareRow,

  } = tableInstance

  return (
    <Container style={{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center', 
      height: '150vh'
    }}>
      <table {...getTableProps()} style={{ border: 'none' }}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th
                  {...column.getHeaderProps()}
                  style={{
                    padding: '12px',
                    color: 'black',
                    borderBottom: '1px solid var(--orange-100)',
                    paddingBottom: '1rem',
                    fontSize: '0.8rem',
                    fontWeight: 'bold',
                  }}
                >
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {rows.map(row => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <td
                      {...cell.getCellProps()}
                      style={{
                        padding: '10px',
                        borderBottom: '1px solid gray',
                        background: 'white',
                        fontSize: '0.75rem'
                        //color: 'var(--gray-300)',

                      }}
                    >
                      {cell.render('Cell')}
                    </td>
                  )
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
    </Container>
  )
}
