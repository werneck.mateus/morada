import type { AppProps } from 'next/app';
import { GlobalStyle } from '../../styles/global';
import { Footer } from '../components/Footer';
import { Header } from '../components/Header';
import 'bootstrap/dist/css/bootstrap.min.css';
function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
          <GlobalStyle/>
          <Header />
          <Component {...pageProps} />
          <Footer />
    </>
  );
}
export default MyApp
