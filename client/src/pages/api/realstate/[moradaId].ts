import { NextApiRequest, NextApiResponse } from "next";
import { api } from "../../../services/api";


export default async (req: NextApiRequest, res: NextApiResponse) => {
   const id = req.query?.moradaId
   if(id !== 'undefined') {
      const data = (await api.get(`realstates/realstate/${id}`)).data
      return res.status(200).send(data)
   }
   return res.status(200).send({} as RealState)
  
  
}