import { NextApiRequest, NextApiResponse } from "next";
import { api } from "../../../../services/api";


export default async (req: NextApiRequest, res: NextApiResponse) => {
   const data = (await api.get('realstates')).data
   return res.status(200).send(data)
}