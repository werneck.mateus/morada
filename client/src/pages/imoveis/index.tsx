import { useRouter } from "next/router"
import React, { useEffect, useState } from "react"
import { Button } from "react-bootstrap"
import { AiFillEdit } from "react-icons/ai"
import { FaTrash } from 'react-icons/fa'
import { PageContainer } from "../../components/Container"
import { TableData } from "../../components/TableData/TableData"
import { local } from "../../services/api"
import { treatBooleanToString } from "../../util/Boolean"
import { treatDateBr } from "../../util/Date"

export default function RealState() {
    const [realstates, SetRealState] = useState<RealState[]>([])
    const [deleted, setDelete] = useState<number[]>([])
    const router = useRouter()

    async function loadRealStates() {
        const realStates = (await local.get('realstates/all')).data as RealState[]
        SetRealState(realStates)
    }

    useEffect(() => {
        loadRealStates()
    }, [])

    useEffect(() => {
        deleted.length && loadRealStates()
    }, [deleted])

    const columns = React.useMemo(() => [
        {
            Header: "ID",
            accessor: 'moradaId'
        },
        {
            Header: "Nome",
            accessor: 'name'
        },
        {
            Header: "Data de Início",
            accessor: 'expectedStartDate'
        },
        {
            Header: "Data de Término",
            accessor: 'expectedEndDate',
        },
        {
            Header: "Desconto",
            accessor: 'hasDiscount',
            Cell: (props: any) => getCustomBooleanCell(props)
        },
        {
            Header: "ITBI",
            accessor: 'hasITBI',
            Cell: (props: any) => getCustomBooleanCell(props)

        },
        {
            Header: "Registros",
            accessor: 'hasRecord',
            Cell: (props: any) => getCustomBooleanCell(props)
        },
        {
            Header: "",
            accessor: 'options'
        }
    ],
        []
    )
    return (
        <>
            <PageContainer>
                <Button
                    variant="success"
                    size='sm'
                    style={{
                        marginBottom: '1rem',
                        marginRight: '1rem'
                    }}
                    onClick={onRefreshClick}
                >
                    Recarregar
                </Button>
                <Button
                    variant="danger"
                    size='sm'
                    style={{ marginBottom: '1rem' }}
                    onClick={onDeleteClick}
                >
                    Apagar Tudo
                </Button>
            </PageContainer>
            <PageContainer>
                {realstates.length ?
                    <TableData columns={columns} data={realstates.map(realstate => (
                        {
                            ...realstate,
                            expectedEndDate: treatDateBr(realstate.expectedEndDate),
                            expectedStartDate: treatDateBr(realstate.expectedStartDate),
                            hasDiscount: treatBooleanToString(realstate.hasDiscount),
                            hasITBI: treatBooleanToString(realstate.hasITBI),
                            hasRecord: treatBooleanToString(realstate.hasRecord),
                            options: getOptions(String(realstate.moradaId))
                        }
                    ))} />
                    : <></>}
            </PageContainer>
        </>
    )


    function getCustomBooleanCell(props: any) {
        return (
            <span style={{
                color: props.cell.value === 'Não' ? '#DC143C' : 'green',
                fontSize: '0.75rem',
            }}>
                {String(props.cell.value)}
            </span>
        )
    }


    async function onEdit(e: any) {
        const id = e.currentTarget.id
        router.push(`/imovel-alterar/${id}`)
    }

    async function onDelete(e: any) {
        const id = e.currentTarget.id
        const deletedRealState = (await local.delete(`/realstate/delete/${id}`)).data
        if (deleted) {
            setDelete([...deleted, deletedRealState])
        }

    }

    async function onDeleteClick() {
       await local.delete(`/realstates/delete`)
       SetRealState([])
    }

    async function onRefreshClick() {
        const realStates = (await local.get(`/realstates/create`)).data as RealState[]
        SetRealState(realStates)
    }

    function getOptions(id: string) {
        return (
            <>
                <div>
                    <Button variant="outline-info" id={id} onClick={onEdit} style={{ marginRight: '0.5rem' }} size='sm'><AiFillEdit size={12} /></Button>
                    <Button variant="outline-danger" id={id} onClick={onDelete} size='sm'><FaTrash size={12} /></Button>
                </div>
            </>
        )
    }
}