import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { Button } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { PageContainer } from "../../components/Container"
import { Input } from "../../components/Input"
import { local } from "../../services/api"
import { treatBooleanToString } from "../../util/Boolean"
import { treatMoneyToReal } from "../../util/Money"

export default function UpdateRealState() {
    const router = useRouter()
    const { moradaId } = router.query
    const [realState, setRealState] = useState<RealState>()
    const { register, handleSubmit, watch, formState: { errors } } = useForm()

    useEffect(() => {
        async function loadRealState() {
            const realstate = (await local.get('realstate/' + moradaId)).data
            setRealState(realstate)
        }
        moradaId && loadRealState()
    }, [moradaId])

    return (
        <PageContainer>
            {realState ?
                <form onSubmit={handleSubmit(data => console.log(data))}>
                    <Input defaultValue={realState.name} label="Nome" register={register}/>
                    <Input defaultValue={treatMoneyToReal(realState.config.price)} label="Preço" register={register}/>
                    <Input defaultValue={String(realState.config.totalArea)} label="Área Total (m2)" register={register}/>
                    <Input defaultValue={realState.status?.name} label="Status" register={register}/>
                    <Input defaultValue={String(realState.config.bedrooms)} label="Quartos" register={register}/>
                    <br />
                    <br />
                    <Input defaultValue={String(realState.config.bathrooms)} label="Banheiro" register={register}/>
                    <Input defaultValue={treatBooleanToString(realState.hasDiscount)} label="Desconto" register={register}/>
                    <Input defaultValue={treatBooleanToString(realState.hasITBI)} label="ITBI" register={register}/>
                    <Input defaultValue={treatBooleanToString(realState.hasRecord)} label="Registros" register={register}/>
                    <br />
                    <br />
                    <br />
                    <div style={{ display: 'flex', alignItems: 'right', justifyContent: 'right' }}>
                        <Button variant="info" type="submit">Salvar</Button>
                    </div>
                </form>
                : <></>}
        </PageContainer>
    )
}