import Head from "next/head";
import { PageContainer } from "../components/Container";

export default function Home() {
  return (
      <>
      <Head>
        <title>Morada</title>
      </Head>
      <PageContainer>
        <img 
        src='Morada.jpg'
        />
      </PageContainer>
    </>
  );
}
