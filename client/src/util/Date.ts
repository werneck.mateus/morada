import moment from "moment";

export function treatDateBr(value: Date) {
    return moment(value, "YYYY-MM-DD").format("DD/MM/YYYY")
}