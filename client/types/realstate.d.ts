declare type RealState = {
    moradaId: number
    statusId: number
    name: string
    expectedStartDate: Date
    expectedEndDate: Date
    hasDiscount: boolean
    hasITBI: boolean
    hasRecord: boolean
    address:RealStateAddress
    config: RealStateConfig
    status: RealStateStatus
  }

  declare type RealStateConfig = {
    moradaId: number
    realStateId: number
    price: number
    bathrooms: number
    bedrooms: number
    totalArea: number
  }

  declare type RealStateAddress = {
    moradaId: number
    realStateId: number
    addressType: string
    address: string
    number: number
    blockNumber: number
    neighborhood: string
    city: string
    cep: string
  }

  declare type RealStateStatus = {
    moradaId: number
    name: string
  }