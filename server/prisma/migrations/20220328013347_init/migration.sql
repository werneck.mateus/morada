-- CreateTable
CREATE TABLE "RealState" (
    "id_realstate" SERIAL NOT NULL,
    "fk_id_status_realstate" INTEGER NOT NULL,
    "name_realstate" VARCHAR(50) NOT NULL,
    "expected_startdate_realstate" DATE NOT NULL,
    "expected_enddate_realstate" DATE NOT NULL,
    "discount_realstate" BOOLEAN NOT NULL,
    "itbi_realstate" BOOLEAN NOT NULL,
    "record_realstate" BOOLEAN NOT NULL,

    CONSTRAINT "RealState_pkey" PRIMARY KEY ("id_realstate")
);

-- CreateTable
CREATE TABLE "RealStateConfig" (
    "id_realstate_config" SERIAL NOT NULL,
    "fk_id_realstate_config" INTEGER NOT NULL,
    "value_realstate" INTEGER NOT NULL,
    "number_bathroom_realstate" INTEGER NOT NULL,
    "number_bedroom_realstate" INTEGER NOT NULL,
    "total_area_realstate " INTEGER NOT NULL,

    CONSTRAINT "RealStateConfig_pkey" PRIMARY KEY ("id_realstate_config")
);

-- CreateTable
CREATE TABLE "RealStateAddress" (
    "id_address" SERIAL NOT NULL,
    "fk_id_realstate_address" INTEGER NOT NULL,
    "addresstype_address" VARCHAR(50) NOT NULL,
    "address_address " VARCHAR(255) NOT NULL,
    "number_address " INTEGER NOT NULL,
    "blocknumber_address" INTEGER NOT NULL,
    "neighborhood_address" VARCHAR(50) NOT NULL,
    "city_address " VARCHAR(50) NOT NULL,
    "cep_address" VARCHAR(10) NOT NULL,

    CONSTRAINT "RealStateAddress_pkey" PRIMARY KEY ("id_address")
);

-- CreateTable
CREATE TABLE "RealStateStatus" (
    "id_realstate_status" SERIAL NOT NULL,
    "name_realstate_status" VARCHAR(50) NOT NULL,

    CONSTRAINT "RealStateStatus_pkey" PRIMARY KEY ("id_realstate_status")
);

-- CreateIndex
CREATE UNIQUE INDEX "RealState_id_realstate_key" ON "RealState"("id_realstate");

-- CreateIndex
CREATE UNIQUE INDEX "RealStateConfig_fk_id_realstate_config_key" ON "RealStateConfig"("fk_id_realstate_config");

-- CreateIndex
CREATE UNIQUE INDEX "RealStateAddress_fk_id_realstate_address_key" ON "RealStateAddress"("fk_id_realstate_address");

-- CreateIndex
CREATE UNIQUE INDEX "RealStateStatus_name_realstate_status_key" ON "RealStateStatus"("name_realstate_status");

-- AddForeignKey
ALTER TABLE "RealState" ADD CONSTRAINT "RealState_fk_id_status_realstate_fkey" FOREIGN KEY ("fk_id_status_realstate") REFERENCES "RealStateStatus"("id_realstate_status") ON DELETE NO ACTION ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "RealStateConfig" ADD CONSTRAINT "RealStateConfig_fk_id_realstate_config_fkey" FOREIGN KEY ("fk_id_realstate_config") REFERENCES "RealState"("id_realstate") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "RealStateAddress" ADD CONSTRAINT "RealStateAddress_fk_id_realstate_address_fkey" FOREIGN KEY ("fk_id_realstate_address") REFERENCES "RealState"("id_realstate") ON DELETE CASCADE ON UPDATE CASCADE;
