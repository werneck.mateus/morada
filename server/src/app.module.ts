import { RealStateModule } from './infrastructure/database/modules/repositories/realstate.module';
import { NestModule, MiddlewareConsumer, Module } from '@nestjs/common';
import { HealthCheckModule } from './infrastructure/helpers/modules/healthcheck.module';

@Module({
  imports: [
    RealStateModule,
    HealthCheckModule
  ],

})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
  }
}
