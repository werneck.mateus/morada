

import { Controller, Get } from '@nestjs/common';
import { PrismaService } from '../../infrastructure/database/services/prisma.service';



@Controller()
export class HealthCheckController {
    constructor(private prisma: PrismaService) { }


    @Get('health-check')
    async healthCheck(): Promise<any> {
        return {
            databaseConnection: await this.testDBConnection(),
            ...await this.testFilePermissions()
        }
    }

    async testDBConnection(): Promise<boolean> {
        let dbtest
        try {
            dbtest = await this.prisma.realState.count()
        } catch (error) {
            dbtest = 'error'
        }
        return typeof dbtest === 'number'
    }

    async testFilePermissions() {
        const fs = require('fs')
        const path = require('path')
        var filePermissions = { createFile: true, deleteFile: true }
        const fileName = path.resolve(process.cwd() + '/files/healthcheck.txt')
        await fs.writeFile(fileName, 'HealthCheck Test', err => {
            if (err) {
                filePermissions.createFile = false;
            } else {
                fs.unlink(fileName, err => {
                    if (err) {
                        filePermissions.deleteFile = false
                    }
                })
            }
        })

        return filePermissions
    }
}
