import { Test } from '@nestjs/testing';
import { RealStateController } from './realstate.controller';
import { PrismaService } from '../../infrastructure/database/services/prisma.service';
import { treatDatesFromRealStatePeriod } from '../../infrastructure/helpers/treatments/Date/treatDate';
import moment from 'moment';
import { RealStateService } from '../services/realstate.service';
import { DatesDto } from '../../infrastructure/helpers/interfaces/date.dto';

describe('RealStateController', () => {
    let realstateController: RealStateController;
    let realstateService: RealStateService;
    let prisma: PrismaService;

    beforeEach(async () => {
        const moduleRef = await Test.createTestingModule({
            controllers: [RealStateController],
            providers: [RealStateService, PrismaService],
        }).compile();

        realstateController = moduleRef.get<RealStateController>(RealStateController);
        realstateService = moduleRef.get<RealStateService>(RealStateService);
        prisma = moduleRef.get<PrismaService>(PrismaService);
    });

    describe('findAll', () => {
        it('É necessário retornar um array de RealState com tamanho 19', async () => {
            expect(await realstateController.findAll()).toHaveLength(19)
        })
    })

    describe('create', () => {
        it('É necessário retornar um array de RealState com tamanho 19 após popular tabela', async () => {
            expect(await realstateController.create()).toHaveLength(19)
        })
    })

    describe('delete', () => {
        it('É necessário retornar o número de RealState deletados igual ao número de registros encontrados anteriormente', async () => {
            const rows = await realstateController.findAll()
            const result = await realstateController.deleteAll()
            expect(result).toBe(rows.length)
            await realstateController.create()
        })
    })

    describe('findByCity', () => {
        it('É necessário retornar um array de RealState apenas com as cidades especificadas. Testa uma cidade aleatória', async () => {
            const city = await getRandomCity()
            const result = await realstateController.findByCity(city)
            const cities = await getRealStatesCities(result)
            expect(cities).toEqual(city.split(','))

        })
    })

    describe('findByEachCity', () => {
        it('É necessário retornar um array de RealState apenas com as cidades especificadas. Testa todas as cidades encontrada no banco', async () => {
            const cities = await getCities()
            for (const city of cities) {
                const result = await realstateController.findByCity(city)
                const foundCities = await getRealStatesCities(result)
                expect(foundCities).toEqual(city.split(','))
            }
        })
    })

    describe('findByPeriod', () => {
        it('É necessário retornar um array de RealState que tem previsao de estar pronto no periodo especificado. Testa um periodo aleatorio', async () => {
            const period = await getRandomPeriod()
            const result = await realstateController.findByPeriod(period)
            const checkDates = await checkPeriodExpectedDateLowerThanMax(result, period, isFutureLookUp(period))
            expect(checkDates).toBe(true)
        })
    })

    describe('findByEachPeriod', () => {
        it('É necessário retornar um array de RealState que tem previsao de estar pronto no periodo especificado. Testa para todos os periodos especificados', async () => {
            const periods = await getPeriods()
            for (const period of periods) {
                const result = await realstateController.findByPeriod(period)
                const checkDates = await checkPeriodExpectedDateLowerThanMax(result, period, isFutureLookUp(period))
                expect(checkDates).toBe(true)
            }
        })
    })

    describe('findByDates', () => {
        it('É necessário retornar um array de RealState que tem previsao de estar pronto no intervalo de datas especificado. Testa um intervalo de datas baseado em um período aletário. Ex: Pronto, Até 12 meses', async () => {
            const period = await getRandomPeriod()
            const interval = await getRandomDateInterval(period)
            const result = await realstateService.findByDate(interval)
            const checkDates = await checkExpectedDatesLowerThanMax(result, interval, isFutureLookUp(period))
            expect(checkDates).toBe(true)
        })
    })

    describe('findByMultipleDateIntervals', () => {
        it('É necessário retornar um array de RealState que tem previsao de estar pronto no intervalo de datas especificado. Testa todos os intervalos baseado nos períodos especificados. Ex: Pronto, Até 12 meses.', async () => {
            const periods = await getPeriods()
            for (const period of periods) {
                const interval = await getRandomDateInterval(period)
                const result = await realstateService.findByDate(interval)
                const checkDates = await checkExpectedDatesLowerThanMax(result, interval, isFutureLookUp(period))
                expect(checkDates).toBe(true)
            }
        })
    })

    async function getCities(): Promise<string[]> {
        const getCities = await prisma.realStateAddress.groupBy({ by: ['city'] })
        const cities = getCities.map((city) => city.city)
        return cities
    }

    async function getRandomCity() {
        const cities = await getCities()
        if (cities?.length) {
            return cities[Math.floor(Math.random() * cities.length)]
        }
        return 'Belo Horizonte'
    }

    async function getRealStatesCities(realstates: Array<any>) {
        const cities = realstates.map(realstate => realstate.address.city)
        return [...new Set(cities)]
    }

    async function getPeriods(): Promise<Array<string>> {
       return ['Pronto', 'Até 12 Meses', 'Até 24 Meses', 'Até 36 Meses', 'Mais de 36 Meses'];
    }

    async function getRandomPeriod() {
        const periods = await getPeriods()
        return periods[Math.floor(Math.random() * periods.length)]
    }

    async function getRandomDateInterval(period: string): Promise<DatesDto> {
        const date = treatDatesFromRealStatePeriod(period)
        const dateString = moment(Object.values(date)[0]).format('YYYY-MM-DD')
        if (period === "Mais de 36 Meses") {
            return { initialDate: dateString }
        }
        return { initialDate: moment().format("YYYY-MM-DD"), finalDate: dateString }

    }


    async function checkPeriodExpectedDateLowerThanMax(realstates: Array<any>, period: string, isFutureLookUp: boolean = false): Promise<boolean> {
        let maxDate = Object.values(treatDatesFromRealStatePeriod(period))[0]
        maxDate = moment(maxDate, 'YYYY-MM-DD').format('YYYY-MM-DD')

        for (const realstate of realstates) {
            const expectedDate = moment(realstate.expectedEndDate, 'YYYY-MM-DD').format('YYYY-MM-DD')
            if (!isFutureLookUp && moment(expectedDate).isAfter(moment(maxDate))) {
                return false
            }
            if (isFutureLookUp && moment(expectedDate).isBefore(moment(maxDate))) {
                return false
            }
        }
        return true
    }

    async function checkExpectedDatesLowerThanMax(realstates: Array<any>, interval: DatesDto, isFutureLookUp: boolean = false): Promise<boolean> {
        for (const realstate of realstates) {
            const expectedDate = moment(realstate.expectedEndDate, 'YYYY-MM-DD').format('YYYY-MM-DD')
            if (!isFutureLookUp && !moment(expectedDate).isBetween(interval.initialDate, interval.finalDate)) {
                return false
            }
            if (isFutureLookUp && moment(expectedDate).isBefore(moment(interval.initialDate))) {
                return false
            }
        }
        return true
    }

    function isFutureLookUp(period: string): boolean {
        return period === "Mais de 36 Meses"
    }
});
