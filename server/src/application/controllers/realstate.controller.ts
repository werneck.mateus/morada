import { DatesDto } from '../../infrastructure/helpers/interfaces/date.dto';
import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Res, ValidationPipe } from '@nestjs/common';
import { RealState } from '@prisma/client';
import { RealStateService } from '../services/realstate.service';
import { Response } from 'express';


@Controller('realstates')
export class RealStateController {
    constructor(private realstateService: RealStateService) { }


    @Get()
    async findAll(): Promise<RealState[]> {
        const realstates = await this.realstateService.findAll()
        return realstates
    }

    @Get('realstate/:id')
    async findByMoradaId(@Param('id') id: string): Promise<RealState> {
        if(id === 'undefined') {
            return {} as RealState
        }
        const realstate = await this.realstateService.findOne(parseInt(id))
        return realstate
    }

    @Get('/populate-table')
    async create(): Promise<RealState[]> {
        const realstates = await this.realstateService.populateTable()
        return realstates
    }

    @Get('filter-city/:city')
    async findByCity(@Param('city') city: string): Promise<RealState[]> {
        const cities = city.split(",")
        return this.realstateService.findByCities(cities)
    }
    @Get('/filter/status-by-period/:period')
    async findByPeriod(@Param('period') period: string): Promise<any> {
        return this.realstateService.findByPeriod(period)
    }

    @Post('/filter/status-by-date')
    async findByDate(@Body(new ValidationPipe({transform: true})) dates: DatesDto, @Res() res: Response): Promise<any> {
      const realstates = await this.realstateService.findByDate(dates)
      res.status(HttpStatus.OK).json(realstates)
    }

    @Delete()
    async deleteAll(): Promise<number> {
        return await this.realstateService.truncateTable()
    }

    @Delete('delete/:id')
    async deleteOne(@Param('id') id: string): Promise<number> {
        if(id === 'undefined') {
            return 0
        }
        return await this.realstateService.deleteById(parseInt(id))
        
    }

   
}
