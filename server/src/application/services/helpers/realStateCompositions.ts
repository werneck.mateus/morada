import path from "path"

export function findRealStateIdByName(realstate, realstates) {
    for (const element of realstates) {
        if (realstate.name == element.name) {
            return element.moradaId
        }
    }
}

export function getSheetFileName() {
    return path.resolve(process.cwd() + '/files/Imoveis.xlsx')
}

export function getCsvColumns() {
    const csvColumns = {
        name: "Nome",
        expectedStartDate: 'PrevisaoInicio',
        expectedEndDate: 'PrevisaoFim',
        hasDiscount: 'Desconto',
        hasITBI: 'ITBI',
        hasRecord: 'Registro',
        address: {
            addressType: 'TipoLogradouro',
            address: 'NomeLogradouro',
            number: 'Numero',
            blockNumber: 'Condominio',
            neighborhood: 'Bairro',
            city: 'Cidade',
            cep: 'CEP',
        },
        config: {
            totalArea: 'Area',
            price: 'Valor',
            bathrooms: 'QtdeBanheiros',
            bedrooms: 'Quartos',
        },
        status: {
            name: 'Status'
        }
    }
    return csvColumns
}

export function getCleanRealStates(realstates: any[]) {
    for (const realstate of realstates) {
        delete realstate['address']
        delete realstate['config']
        delete realstate['status']
    }
    return realstates
}
