import { RealState, RealStateAddress, RealStateConfig, RealStateStatus } from "@prisma/client";
import { treatDatesFromRealStatePeriod } from "../../../infrastructure/helpers/treatments/Date/treatDate";
import { SpreadSheet } from "../../../infrastructure/helpers/treatments/SpreadSheet/spreadsheet";
import { removeWhiteSpaceAtTheEnd } from "../../../infrastructure/helpers/treatments/String/treatString";
import { PrismaService } from "../../../infrastructure/database/services/prisma.service";
import { RealStateService } from "../realstate.service";
import { getSheetFileName, getCsvColumns, findRealStateIdByName, getCleanRealStates } from "./realStateCompositions";

export class RealStateHelper {

    private status: RealStateStatus[];
    private config: RealStateConfig[];
    private address: RealStateAddress[];

    constructor(private prisma: PrismaService
    ) {
        this.status = []
        this.config = []
        this.address = []
    }

    async createDataFromSpreadSheetFile(): Promise<Array<RealState>> {
        const sheetData = await SpreadSheet.getImportDataFromSheet(getSheetFileName(), getCsvColumns())
        await this.createManyFromSheetData(sheetData)
        const realstates = await this.prisma.realState.findMany({...RealStateService.relations})
        await this.createRealStateRelationshipsFromSheetData(sheetData, realstates)
        return await this.prisma.realState.findMany({ ...RealStateService.relations, orderBy:{moradaId: 'asc'}})
    }

    async createManyFromSheetData(data: RealState[]): Promise<void> {
        const clonedData = await data.map(a => Object.assign({}, a))
        await this.createRealStateStatusFromSheetData(clonedData)
        await this.prisma.realState.createMany({ data: clonedData })
    }

    async createRealStateStatusFromSheetData(data: Array<any>): Promise<void> {
        const repeated = []
        for (const realstate of data) {
            const newStatus = { name: realstate.status.name } as RealStateStatus
            if (!repeated.includes(newStatus.name)) {
                const created = await this.prisma.realStateStatus.create({ data: newStatus })
                this.status.push(created)
                repeated.push(newStatus.name)
                continue
            }
        }
        this.appendStatusIdToRealStates(data)

    }

    appendStatusIdToRealStates(data: Array<any>): Array<RealState> {
        for (const realState of data) {
            realState['statusId'] = this.getStatusIdFromStatusName(realState.status.name)
        }
        return getCleanRealStates(data)
    }

    getStatusIdFromStatusName(name: string): number {
        for (const status of this.status) {
            if (status.name === name) {
                return status.moradaId
            }
        }
        return undefined
    }

    async saveRelationships(): Promise<void> {
        await this.prisma.realStateAddress.createMany({ data: this.address })
        await this.prisma.realStateConfig.createMany({ data: this.config })
    }

    async createRealStateRelationshipsFromSheetData(data: Array<any>, realstates: Array<RealState>): Promise<void> {
        for (const realstate of data) {
            const realStateId = findRealStateIdByName(realstate, realstates)
            this.address.push({ realStateId: realStateId, ...realstate.address })
            this.config.push({ realStateId: realStateId, ...realstate.config })
        }
        await this.saveRelationships()
    }

     getPeriodFilter(period: string) {
        const date = treatDatesFromRealStatePeriod(period)
        const operator = Object.keys(date)[0]
        const filter = { where: { expectedEndDate: { [operator]: date[operator] } } }
        return filter
    }

    getCitiesFilter(cities: string[]) {
        let or = []
        for (const city of cities) {
            let newCity = removeWhiteSpaceAtTheEnd(city)
            if (newCity && newCity !== '') {
                or.push({ address: { is: { city: { contains: newCity, mode: 'insensitive' } } } })
            }
        }
        return { where: { OR: or } }
    }
}



