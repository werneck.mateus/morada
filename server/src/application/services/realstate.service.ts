import { DatesDto } from './../../infrastructure/helpers/interfaces/date.dto';
import { Injectable } from "@nestjs/common";
import { RealState } from '@prisma/client';
import { PrismaService } from "../../infrastructure/database/services/prisma.service";
import { treatStringToDate } from '../../infrastructure/helpers/treatments/Date/treatDate';
import { RealStateHelper } from './helpers/realstatehelper';

@Injectable()
export class RealStateService {
    public static relations = { include: { address: true, config: true, status: true } };
    constructor(private prisma: PrismaService) { }

    async findAll(): Promise<RealState[]> {
        const realstates = await this.prisma.realState.findMany({ ...RealStateService.relations, orderBy: { moradaId: 'asc' } })
        if (!realstates?.length) {
            return await this.populateTable()
        }
        return realstates
    }

    async populateTable(): Promise<RealState[]> {
        const realstateHelper = new RealStateHelper(this.prisma)
        await this.truncateTable()
        return await realstateHelper.createDataFromSpreadSheetFile()
    }

    async findByCities(cities: Array<string>): Promise<RealState[]> {
        const realstateHelper = new RealStateHelper(this.prisma)
        const filter = realstateHelper.getCitiesFilter(cities)
        const realstates = await this.prisma.realState.findMany({
            ...RealStateService.relations,
            ...filter,
            orderBy: { moradaId: 'asc' }
        })
        return realstates
    }

    async findByPeriod(period: string): Promise<RealState[]> {
        const realstateHelper = new RealStateHelper(this.prisma)
        const filter = realstateHelper.getPeriodFilter(period)
        const realstates = await this.prisma.realState.findMany({
            ...RealStateService.relations,
            ...filter,
            orderBy: [{ moradaId: 'asc' }, { expectedEndDate: 'asc' }]
        })
        return realstates
    }

    async findByDate(dates: DatesDto) {
        return await this.prisma.realState.findMany({
            ...RealStateService.relations,
            where: {
                AND: [
                    dates.initialDate && { expectedEndDate: { gte: treatStringToDate(dates.initialDate) } },
                    dates.finalDate && { expectedEndDate: { lte: treatStringToDate(dates.finalDate) } },
                ]
            },
            orderBy: [{ moradaId: 'asc' }, { expectedEndDate: 'asc' }]
        })
    }

    async truncateTable(): Promise<number> {
        const { count } = await this.prisma.realState.deleteMany()
        await this.prisma.realStateStatus.deleteMany()
        return count
    }

    async findOne(id: number): Promise<RealState> {
        const realstate = await this.prisma.realState.findUnique({ ...RealStateService.relations, where: { moradaId: id } })
        return realstate
    }

    async deleteById(id: number): Promise<number> {
        const { moradaId } = await this.prisma.realState.delete({ where: { moradaId: id } })
        return moradaId
    }

}