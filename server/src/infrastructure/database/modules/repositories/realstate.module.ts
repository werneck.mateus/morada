
import { Module } from '@nestjs/common';
import { RealStateService } from '../../../../application/services/realstate.service';
import { RealStateController } from '../../../../application/controllers/realstate.controller';
import { PrismaModule } from '../prisma.module';


@Module({
    imports:[PrismaModule],
    controllers:[RealStateController],
    providers:[RealStateService],
})
export class RealStateModule {}
