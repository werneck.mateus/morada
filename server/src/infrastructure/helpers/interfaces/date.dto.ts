import { IsDateString, IsOptional } from "class-validator";

export class DatesDto {
    @IsDateString()
    @IsOptional()
    initialDate?: string

    @IsDateString()
    @IsOptional()
    finalDate?: string

}