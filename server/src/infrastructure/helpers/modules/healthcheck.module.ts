import { HealthCheckController } from './../../../application/controllers/health-check.controller';
import { Module } from '@nestjs/common';
import { PrismaModule } from '../../../infrastructure/database/modules/prisma.module';

@Module({
    imports: [PrismaModule],
    controllers:[HealthCheckController]
})
export class HealthCheckModule {}
