export function treatStringToBoolean(value: string) {
    return value === 'true';
}

export function isStringBoolean(value: string) {
    return value === 'true' || value === 'false'
}