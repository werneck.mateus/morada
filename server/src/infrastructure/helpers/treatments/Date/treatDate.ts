import moment from "moment"
import { exit } from "process"
import { removeWhiteSpaceAtTheEnd } from "../String/treatString"

export function isStringDate(value: string) {
    return value.match(/[0-9][0-9]?[-\/][0-9]{2}[-\/][0-9]{4}|[0-9][0-9]?[-\/][0-9][0-9]?[-\/][0-9]{2}/g)
}

export function treatDateBr(value: string) {
    let date = moment(value, 'DD/MM/YYYY').format("YYYY-MM-DD")
    if (date === "Invalid date") {
        date = moment(value, 'MM/DD/YYYY').format("YYYY-MM-DD")
    }
    return moment(date).toDate()
}

export function treatStringToDate(value: string) {
    return moment(value, 'YYYY-MM-DD').toDate()
}

export function treatDatesFromRealStatePeriod(period: string) {
    period = (removeWhiteSpaceAtTheEnd(period))
    let date;
    let operator = 'lte'
    switch (period) {
        case 'Pronto':
            date = moment().subtract(1, 'd').toDate()
            break
        case 'Até 12 Meses':
            date = moment().add(1, 'y').toDate()
            break
        case 'Até 24 Meses':
            date = moment().add(2, 'y').toDate()
            break
        case 'Até 36 Meses':
            date = moment().add(3, 'y').toDate()
            break
        case 'Mais de 36 Meses':
            date = moment().add(3, 'y').toDate()
            operator = 'gt'
            break
    }
    return { [operator]: date }
}