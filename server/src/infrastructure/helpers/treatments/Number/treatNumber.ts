export function isStringNumeric(value: string) {
    return value.match(/^[0-9]*$/g)
}

export function treatNumber(value: string) {
   if(value.match(/[0-9]+\.[0-9]+/g)) {
       return parseFloat(value)
   }
    return parseInt(value)
}