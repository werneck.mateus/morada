import csvParser from 'csv-parser';
import { createReadStream, unlinkSync } from 'fs';
import path from 'path';
import { readFile, writeFile } from 'xlsx';
import { isStringBoolean, treatStringToBoolean } from '../Boolean/treatBoolean';
import { isStringDate, treatDateBr } from '../Date/treatDate';
import { isStringNumeric, treatNumber } from '../Number/treatNumber';


export class SpreadSheet {
    constructor() { }

    static async getImportDataFromSheet(fileName: string, columnsToMap): Promise<any[]> {
        if (!fileName.match('.csv')) {
            fileName = await SpreadSheet.convertToCSV(fileName)
        }
        const importData = await SpreadSheet.getMappedDataFromCsv(fileName, columnsToMap)
        return importData
    }

    static async convertToCSV(fileName: string): Promise<string> {
        const xlsxFile = readFile(fileName)
        const outputFile = path.resolve(process.cwd() + "/files/imoveis.csv")
        writeFile(xlsxFile, outputFile, { bookType: 'csv' })
        return outputFile
    }

    static getMappedDataFromCsv(csvFile: string, columns, separator = ','): Promise<any[]> {
        const result = []
        const parser = csvParser({ separator: separator })
        return new Promise((resolve, reject) => {
            createReadStream(csvFile)
                .pipe(parser)
                .on('data', (data) => {
                    data = SpreadSheet.removeInvalidCharFromDataKeys(data)
                    const mappedRow = SpreadSheet.getDataFromCsvRow(data, columns)
                    result.push(mappedRow)
                })
                .on('end', () => {
                    unlinkSync(csvFile)
                    resolve(result)
                })
        })
    }

    static getDataFromCsvRow(data, columns) {
        const keys = Object.keys(columns)
        let mapped = {}

        for (const key of keys) {
            if (typeof columns[key] === 'string') {
                let value = data[columns[key]]
                if (isStringBoolean(value)) {
                    value = treatStringToBoolean(value)
                } else if (isStringDate(value)) {
                    value = treatDateBr(value)
                } else if (isStringNumeric(value)) {
                    value = treatNumber(value)
                }
                mapped[key] = value
                continue
            } else if (typeof columns[key] === 'object') {
                mapped[key] = SpreadSheet.getDataFromCsvRow(data, columns[key])
            }
        }
        return mapped
    }

    static removeInvalidCharFromDataKeys(data) {
        let newData = {}
        const keys = Object.keys(data)
        for (const key of keys) {
            if (key.match(/'|"|\s/g)) {
                const newKey = key.replaceAll(/['"\s]+/g, '')
                newData[newKey] = data[key]
                continue
            }
            newData[key] = data[key]
        }
        return newData
    }

}
