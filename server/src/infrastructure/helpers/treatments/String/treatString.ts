export function removeWhiteSpaceAtTheEnd(value: string) {
    const whitespace = value.at(value.length - 1) === " " ? true : false

    if (whitespace) {
        value = value.substring(0, value.length - 1)
    }
    return value
}